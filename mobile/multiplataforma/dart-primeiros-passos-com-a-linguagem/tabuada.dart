void main() {
    // x++ substitui x = x + 1
    // x-- substitui x = x - 1
    // x += y substitui x = x + y
    // x -= y substitui x = x - y
    // x *= y substitui x = x * y
    // x /= y substitui x = x / y

    for (int multiplicando = 1; multiplicando <= 10; multiplicando++) {
        print("Tabuado de $multiplicando");
        for (int contador = 0; contador <= 10; contador++) {
            // int resultado = multiplicando * contador;
            // ${multiplicando * contador}
            print("$multiplicando * $contador = ${multiplicando * contador}");
        }
    }
}