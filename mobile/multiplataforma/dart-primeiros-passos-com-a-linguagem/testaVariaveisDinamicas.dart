void main() {
    // dart fortemente tipado
    int idade = 25;
    double pi = 3.1415;
    String texto = "Eu tenho $idade anos e valor de pi é $pi";

    print(texto);

    // dart tambem consegue detectar o tipo da variavel em execução (javascript)
    // dart pega a var idadeTipo e converter automatico para int (forma dinamica)
    // altera o tipo da variável em tempo de execução
    // mesmo utilizando o var não é possível alterar o tipo da variável já detectado
    var idadeTipo = 25;
    var piTipo = 3.1415;
    var textoTipo = "Eu tenho $idade anos e valor de pi é $pi";

    print(textoTipo);

    // tipo que altera de forma dinâmica o tipo da variável de acordo com o programa
    // a variável detecta o valor 25 como inteiro e depois assume o tipo double
    // custo computacional maior para entender o tipo da variável em determinado momento
    dynamic idadeDynamic = 25;
    idadeDynamic = 25.5;

    print(idadeDynamic);
}