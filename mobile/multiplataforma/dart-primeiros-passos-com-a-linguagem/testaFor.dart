void main() {
    // for ( variavel; condição; o que faz com essa variavel)
    // ++ = +1
    // -- = -1

    // loop infinito
    // for (;;) {
    // }

    for (int contador = 0; contador <= 10; contador++) {
        print("O valor do contador é $contador");
    }
}