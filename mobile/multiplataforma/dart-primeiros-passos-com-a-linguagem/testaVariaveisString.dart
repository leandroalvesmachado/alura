void main() {
    // para criar uma var string
    // pode ser aspa '' ou ""
    int ano = 2013;
    String texto = "Alura - Cursos Online de Tecnologia desde $ano.";

    // imprime a string
    print(texto);
    print("$texto");
    print("$texto $ano");
}