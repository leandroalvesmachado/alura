void main() {
    // converte de double para int
    double pi = 3.141592;
    int piInteiro = pi.toInt();

    // converte de int para double
    int dezena = 100;
    double dezenaDouble = dezena.toDouble();

    // converte de string para int
    String stringInteiro = "13";
    int numeroInteiro = int.parse(stringInteiro);

    // converte de string para double
    String stringDouble = "13.14";
    double numeroDouble = double.parse(stringDouble);
}