void main() {
    // para criar uma var real
    double salario = 1250.50;
    double salarioMultiplicado = 1250.50 * 2;

    // imprime a string
    print("Meu salário é 1250.50");
    
    // imprime a variavel
    print(salario);

    // imprime a variavel junto com a string
    print("Meu novo salário é $salarioMultiplicado.");
}