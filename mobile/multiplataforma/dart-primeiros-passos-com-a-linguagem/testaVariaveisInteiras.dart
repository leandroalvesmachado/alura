void main() {
    // para criar uma var inteira
    int idade = 25;
    int idadeIrmao = idade + 2;
    int idadeAvo = (idade + idadeIrmao) * 3;

    // imprime a string
    print("Eu tenho 25 anos.");
    
    // imprime a variavel
    print(idade);

    // imprime a variavel junto com a string
    print("Eu tenho $idade anos.");
    print("Meu irmão têm $idadeIrmao anos.");
    print("Meu avô têm $idadeAvo anos.");
}