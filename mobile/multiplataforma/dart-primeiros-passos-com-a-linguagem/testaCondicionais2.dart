void main() {
    print("Testando condicionais");

    int idade = 17;
  
    // true significa verdadeiro
    // false significa falso
    // tipo bool usado para variaveis com valor true ou false

    bool maior_idade = idade >= 18;
    bool acompanhado = false;

    print(maior_idade);

    if(maior_idade) {
        print("Você pode entrar!");
    } else {
        if(acompanhado) {
            print("Você é menor de idade, mas está acompanhado, pode entrar!");
        } else {
            print("Você não pode entrar!");
        }
    }
}