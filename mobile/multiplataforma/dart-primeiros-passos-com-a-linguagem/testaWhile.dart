void main() {
    int contador = 0;

    // != diferente
    
    // vai de 0 até 0 9
    while (contador != 10) {
        print("O valor do contador é $contador");
        contador++;
    }

    // faça enquanto for <= 10
    do {
        print("O valor do contador é $contador");
        contador++;
    } while (contador <= 10);

    // comandos acima são diferentes
    // o segundo faz e depois que checa
    // o primeiro checa antes de fazer

    print("Finalizando o programa");
}