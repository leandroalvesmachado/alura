#### Exibir containers ativos
```sh
$ docker ps
```

#### Exibir containers existentes (parados)
```sh
$ docker ps -a
```
* CONTAINER ID IMAGE COMMAND CREATED STATUS PORTS NAMES
c16a8b231f1f hello-world "/hello" 41 minutes ago Exited (0) 41 minutes ago admiring_galois

#### Atrelar terminal a imagem
```sh
$ docker run -it nome_container
```

#### Start no container
```sh
$ docker start CONTAINER_ID
```

#### Stop no container
```sh
$ docker stop CONTAINER_ID
```

#### Start no container com modo atrelado
```sh
$ docker start -a -i CONTAINER_ID
```

#### Deletar container pelo ID
```sh
$ docker rm CONTAINER_ID
```

#### Deletar containers inativos
```sh
$ docker container prune
```

#### Listar imagens
```sh
$ docker images
```

#### Remover imagem
```sh
$ docker rmi NOME_IMAGEM
```

#### Linkar container com uma porta do pc
```sh
$ docker run -d -p 12345(meu pc porta):80(porta container) CONTAINER (porta especifica)
```
```sh
$ docker run -d -P CONTAINER (porta aleatoria)
```
```sh
$ docker run -d -P --name meu-container CONTAINER (nome para o container rodando)
```
```sh
$ docker run -d -P -e AUTHOR="LEANDRO" --name meu-container CONTAINER (variavel de ambiente)
```

#### Visualizar portas usadas pelo container
```sh
$ docker port CONTAINER_ID
```

#### Se estiver rodando o container numa VM, precisar acessar pelo ip da VM
```sh
$ docker-machine ip
```

#### Resumo

```
```

Aprendemos neste capítulo:

Comandos básicos do Docker para podermos baixar imagens e interagir com o container.
Imagens do Docker possuem um sistema de arquivos em camadas (Layered File System) e os benefícios dessa abordagem principalmente para o download de novas imagens.
Imagens são Read-Only sempre (apenas para leitura)
Containers representam uma instância de uma imagem
Como imagens são Read-Only os containers criam nova camada (layer) para guardar as alterações
O comando Docker run e as possibilidades de execução de um container
Segue também uma breve lista dos comandos utilizados:

docker ps - exibe todos os containers em execução no momento.

docker ps -a - exibe todos os containers, independentemente de estarem em execução ou não.

docker run -it NOME_DA_IMAGEM - conecta o terminal que estamos utilizando com o do container.

docker start ID_CONTAINER - inicia o container com id em questão.

docker stop ID_CONTAINER - interrompe o container com id em questão.

docker start -a -i ID_CONTAINER - inicia o container com id em questão e integra os terminais, além de permitir interação entre ambos.

docker rm ID_CONTAINER - remove o container com id em questão.

docker container prune - remove todos os containers que estão parados.

docker rmi NOME_DA_IMAGEM - remove a imagem passada como parâmetro.

docker run -d -P --name NOME dockersamples/static-site - ao executar, dá um nome ao container.

docker run -d -p 12345:80 dockersamples/static-site - define uma porta específica para ser atribuída à porta 80 do container, neste caso 12345.

docker run -d -P -e AUTHOR="Fulano" dockersamples/static-site - define uma variável de ambiente AUTHOR com o valor Fulano no container criado.