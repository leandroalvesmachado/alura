# Aula 1 - Introdução ao Docker

#### Instalação
```sh
$ sudo snap install docker
```
#### Checar versão
```sh
$ sudo docker version
```
#### Executar docker sem precisar do sudo
```sh
$ sudo usermod -aG docker $(whoami)
```

#### Executar imagem (se não existir, ele baixa a última versão)
```sh
$ docker run hello-world (nome da imagem)
```
* Ex: 
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete 
Digest: sha256:8e3114318a995a1ee497790535e7b88365222a21771ae7e53687ad76563e8e76
Status: Downloaded newer image for hello-world:latest
Hello from Docker!
This message shows that your installation appears to be working correctly.

#### Resumo

```
```