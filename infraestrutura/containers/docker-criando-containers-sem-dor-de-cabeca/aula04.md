# Docker

# Aula 4 - Construindo nossas próprias imagens

#### Criando um Dockerfile

```
FROM node:latest (última versão)

MAINTAINER leandroalvesmachado

ENV NODE_ENV=dev (setando variáveis de ambiente, caso precise)
ENV PORT=3000 (setando variáveis de ambiente, caso precise)

COPY . /var/www (copia da raiz do projeto para pasta /var/www do container)

WORKDIR /var/www (local que os comandos serão executados)

RUN npm install (instalando dependências)

ENTRYPOINT ["npm", "start"] (comando executado assim que o container inicia)

EXPOSE 3000 (porta utilizada pelo container)
ou 
EXPOSE $PORT (variável de ambiente criada com o ENV)
```

```sh
$ docker build -f Dockerfile -t leandroalvesmachado/node . (cria a imagem)
```

#### Subindo a imagem do Docker Hub

```sh
$ docker login (logar no docker hub)
```

```sh
$ docker push NOME_IMAGEM (subindo imagem leandroalvesmachado/node)
```

```sh
$ docker pull NOME_IMAGEM (baixar imagem)
```