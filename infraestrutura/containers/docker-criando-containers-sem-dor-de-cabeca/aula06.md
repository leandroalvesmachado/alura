# Docker

# Aula 6 - Trabalhando com o Docker Compose

#### Entendendo o Docker Compose (docker-compose.yml)

```
```

#### Entendendo a aplicação

```
```

#### Criando o docker-compose.yml

```
version: '3'
services:
    nginx:
        build:
            dockerfile: ./docker/nginx.dockerfile (criando imagem leandroalvesmachado/nginx)
            context: .
        image: leandroalvesmachado/nginx
        container_name: nginx
        ports:
            - "80:80"
        network:
            - production-network
        depends_on:
            - "node1"
            - "node2"
            - "node3"
    mongo:
        image: mongo (usando imagem ja pronto do mongo)
        network:
            - production-network
    node1:
        build:
            dockerfile: ./docker/alura-books.dockerfile
            context: .
        image: leandroalvesmachado/alura-books
        container_name: alura-books1
        ports:
            - "3000"
        network:
            - production-network
        depends_on:
            - "mongodb"
    node2:
        build:
            dockerfile: ./docker/alura-books.dockerfile
            context: .
        image: leandroalvesmachado/alura-books
        container_name: alura-books2
        ports:
            - "3000"
        network:
            - production-network
        depends_on:
            - "mongodb"
    node3:
        build:
            dockerfile: ./docker/alura-books.dockerfile
            context: .
        image: leandroalvesmachado/alura-books
        container_name: alura-books3
        ports:
            - "3000"
        network:
            - production-network
        depends_on:
            - "mongodb"
network:
    production-network:
        driver: bridge
```

#### Subindo os serviços

```sh
$ docker-compose build (realiza apenas o build das imagens)
```

```sh
$ docker-compose up -d (sobe os serviços)
```

```sh
$ docker-compose ps (verifica os serviços subidos pelo docker compose)
```

```sh
$ docker-compose down (para os serviços e remove)
```