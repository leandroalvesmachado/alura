# Docker

# Aula 5 - Comunicação entre containers

#### Networking no Docker

```
Todos os containers ficam na mesma rede (default network)
```

```sh
$ hostname -i (ver ip container)
```

```sh
$ docker network create --driver bridge minha-rede (Criando minha rede)
```

```sh
$ docker network ls (Lista redes criadas)
```

```sh
$ docker run -it --name meu-ubuntu --network minha-rede ubuntu (Iniciando container especificando a rede dele)
```


#### Pegando dados de um banco

```
1 - Baixar imagens (docker pull imagem)
```

#### Resumo

```
```
