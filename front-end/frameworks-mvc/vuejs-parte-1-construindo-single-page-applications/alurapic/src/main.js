// Global View Object
import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';

// quando se usa export const routes, o import fica { routes }
import { routes } from './routes';


// $http
Vue.use(VueResource);
Vue.use(VueRouter);

// incluindo a routes no router
const router = new VueRouter({
    routes, // short for `routes: routes`
    mode: 'history' // remove o # da url do navegador, localmente, necessario configurar no servidor que hospeda tb
});

// View instance
new Vue({
    el: '#app',
    router, // short for `router: router`
    render: h => h(App) // informa que o App.vue deve ser carregado no #app
})

