import Home from './components/home/Home.vue';
// import Cadastro from './components/cadastro/Cadastro.vue';

// carregamento lazy loading, carrega só quando acessado
const Cadastro = () => System.import('./components/cadastro/Cadastro.vue')

export const routes = [
    { path: '', component: Home, name: 'home', titulo: 'Home', menu: true },
    // carregamento lazy loading, carrega só quando acessado outra forma
    // { 
    //     path: '/cadastro', 
    //     // component: Cadastro, 
    //     name: 'cadastro', 
    //     titulo: 'Cadastro', 
    //     menu: true ,
    //     component: () => import('./components/cadastro/Cadastro.vue')
    // },
    // carregamento lazy loading, carrega só quando acessado
    { 
        path: '/cadastro', 
        // component: Cadastro, 
        name: 'cadastro', 
        titulo: 'Cadastro', 
        menu: true ,
        component: Cadastro
    },
    { path: '/cadastro/:id', component: Cadastro, name: 'altera', titulo: 'Cadastro', menu: false },
    // qualquer endereço que nao existe mapeado, joga pro componente especificado
    { path: '*', component: Home, menu: false }
];