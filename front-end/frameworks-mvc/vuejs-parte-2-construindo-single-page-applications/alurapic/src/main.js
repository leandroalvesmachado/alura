// Global View Object
import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';
import msg from './pt_BR';

// importando bootstrap
import 'bootstrap/dist/css/bootstrap.css';
// para usar o js do bootstrap instalar npm install jquery --save , precisa tb configurar webpack
// import 'bootstrap/dist/js/bootstrap.js';


// importando css criado por mim
import './assets/css/teste.css';

// importando js criado por mim
import './assets/js/teste';



// quando se usa export const routes, o import fica { routes }
import { routes } from './routes';

// importanto directives
import './directives/Transform';


// $http
Vue.use(VueResource);
// url root da api para usar em todo o sistema
// Vue.http.options.root= 'http://localhost:3000';
// pegando url da api do webpack.config.json
Vue.http.options.root = process.env.API_URL ? process.env.API_URL : 'http://localhost:3000';

Vue.use(VueRouter);

// incluindo a routes no router
const router = new VueRouter({
    routes, // short for `routes: routes`
    mode: 'history' // remove o # da url do navegador, localmente, necessario configurar no servidor que hospeda tb
});

// para fazer validação nos forms
Vue.use(VeeValidate, {
    locale: 'pt_BR',
    dictionary: {
        pt_BR: {
            messages: msg
        }
    }
});

// View instance
new Vue({
    el: '#app',
    router, // short for `router: router`
    render: h => h(App) // informa que o App.vue deve ser carregado no #app
})

