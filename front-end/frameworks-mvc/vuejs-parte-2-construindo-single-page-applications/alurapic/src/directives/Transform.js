import Vue from 'vue';

// v-meu-transform
Vue.directive('meu-transform', {
    bind(el, binding, vnode) {
        console.log('diretiva associada meu-transform');
        let current = 0;
        el.addEventListener('dblclick', () => {
            // current += 90;

            // let incremento = 0;
            // let animate = false;

            // // usando o parametro passado
            // if (binding.value) {
            //     incremento = binding.value.incremento;
            //     animate = binding.value.animate;
            // }
            
            // current += incremento;

            // // aplicando um css
            // if (animate) el.style.transition = 'transform 0.5s';
            // el.style.transform = `rotate(${current}deg)`;

            let incremento = binding.value || 90;
            let efeito;

            if (!binding.arg || binding.arg == 'rotate') {
                if (binding.modifiers.reverse) {
                    current -= incremento;
                } else {
                    current += incremento;
                }

                efeito = `rotate(${current}deg)`;
            } else if (binding.arg == 'scale'){
                efeito = `scale(${incremento})`;
            }

            if (binding.modifiers.animate) el.style.transition = 'transform 0.5s';
            el.style.transform = efeito;
        });
    }
});