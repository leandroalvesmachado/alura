# Aula transformações em imagens
# cv2 ja vem com numpy
import cv2
import numpy as np
import matplotlib
import tkinter
from matplotlib import pyplot as plt
matplotlib.use('TkAgg')

# RGB = Red Green Blue
# caminho da imagem e leitura da imagem para uma matriz
img_caminho = 'INRIA_Person_Dataset/dadosImagem/Treinamento/positivos/crop_000010.png'
img_teste = cv2.imread(img_caminho)

# <class 'numpy.ndarray'>
# print(type(img_teste))
# (720, 594, 3) dimensao da imagem
# print(img_teste.shape)

# procurando o minimo na imagem
# Minimo : 1 (perto do preto 0)
# Maximo : 255 (branco)
# print("Minimo :", np.min(img_teste))
# print("Maximo :", np.max(img_teste))

# transformando a imagem em tons de cinza
img_teste_cinza = cv2.cvtColor(img_teste, cv2.COLOR_RGB2GRAY)
# cv2.imshow('imagem', img_teste_cinza)
# cv2.waitKey(5000)


# Redimensionando a imagem
# original img_teste.shape = (720, 594)
# INTER_CUBIC = média aritmetica dos vizinhos mais proximos bicubic
img_redimensionada = cv2.resize(img_teste_cinza, (360, 360), interpolation=cv2.INTER_CUBIC)

# Original:  (720, 594)
# Redimensionada:  (360, 360, 3)
print('Original: ', img_teste_cinza.shape)
print('Redimensionada: ', img_redimensionada.shape)

# colocando imagem do lado da outra
# matriz 1 linha por duas colunas
plt.subplot(121)
plt.title('Original')
plt.imshow(img_teste_cinza, cmap='gray', interpolation='bicubic')
plt.subplot(122)
plt.title('Redimensionada')
plt.imshow(img_redimensionada, cmap='gray', interpolation='bicubic')
plt.show()