# Aula Segmentacao: filtros de frequencia e convolucoes
# Funcao remover
# Binarizacao
# Binarizacao com Limiar adaptativo
# Convoluindo sobre uma imagem
import cv2
import numpy as np
import matplotlib
import tkinter
from matplotlib import pyplot as plt
matplotlib.use('TkAgg')

img_caminho = 'INRIA_Person_Dataset/dadosImagem/Treinamento/positivos/crop_000010.png'
img_teste = cv2.imread(img_caminho)

img_teste_cinza = cv2.cvtColor(img_teste, cv2.COLOR_RGB2GRAY)
img_redimensionada = cv2.resize(img_teste_cinza, (360, 360), interpolation=cv2.INTER_CUBIC)
img_teste_equalizada = cv2.equalizeHist(img_redimensionada)

# valor de limiar 127, valor medio entre 0 (preto absoluto) e 255 (branco absoluto)
# todos os valores que forem 127 ficaram 255
valor_retorno, img_binarizada = cv2.threshold(img_teste_equalizada, 127, 255, cv2.THRESH_BINARY)

cv2.imshow('imagem', img_binarizada)
cv2.waitKey(5000)