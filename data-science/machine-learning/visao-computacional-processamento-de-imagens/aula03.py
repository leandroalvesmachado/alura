# Aula ajuste de Brilho e Contraste
# quantificacao de cores
# equalizacao
# visualizacao da equalizacao
import cv2
import numpy as np
import matplotlib
import tkinter
from matplotlib import pyplot as plt
matplotlib.use('TkAgg')

# caminho da imagem e leitura da imagem para uma matriz
img_caminho = 'INRIA_Person_Dataset/dadosImagem/Treinamento/positivos/crop_000010.png'
img_teste = cv2.imread(img_caminho)

# transformando a imagem em tons de cinza
img_teste_cinza = cv2.cvtColor(img_teste, cv2.COLOR_RGB2GRAY)
# redimensionando img_teste_cinza
img_redimensionada = cv2.resize(img_teste_cinza, (360, 360), interpolation=cv2.INTER_CUBIC)

# cv2.imshow('imagem', img_redimensionada)
# cv2.waitKey(5000)

# contando de cores (histograma), quantificacao de cores
# R G B
# 0 = tons de cinza
# 256 = quantidade de cores que quer contar
# 0, 256 conta do 0 ate o 256
histograma = cv2.calcHist([img_redimensionada], [0], None, [256], [0, 256])

# (256, 1)
# print(histograma.shape)

#print(histograma.astype(np.int))

# procurando o minimo na imagem
# Minimo : 12 (perto do preto 0)
# Maximo : 255 (branco)
# print("Minimo :", np.min(img_redimensionada))
# print("Maximo :", np.max(img_redimensionada))

# gera o grafico com o plot
# plt.plot(histograma)
# plt.show()

# separar a pessoa da vegetacao (equalizacao)
# distribuir melhor as cores, tem uma melhor separação dos objetos da imagem
img_teste_equalizada = cv2.equalizeHist(img_redimensionada)
histograma_equalizado = cv2.calcHist([img_teste_equalizada], [0], None, [256], [0, 256])

# 1 linha e 2 colunas grade
plt.figure(figsize=(10, 5))
plt.subplot(121)
plt.title('Histograma Original')
plt.plot(histograma)
plt.subplot(122)
plt.title('Histograma Equalizado')
plt.plot(histograma_equalizado)

plt.figure(figsize=(10, 10))
plt.subplot(121)
plt.title('Imagem Original')
plt.imshow(img_redimensionada, cmap='gray', interpolation='bicubic')
plt.subplot(122)
plt.title('Imagem Equalizada')
plt.imshow(img_teste_equalizada, cmap='gray', interpolation='bicubic')

plt.show()

