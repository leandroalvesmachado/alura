import cv2

# caminho da imagem e leitura da imagem para uma matriz
img_caminho = 'INRIA_Person_Dataset/dadosImagem/Treinamento/positivos/crop_000010.png'
img_teste = cv2.imread(img_caminho)

# abre uma janela com a imagem, waitKey para manter a janela aberta
cv2.imshow('imagem', img_teste)
cv2.waitKey(0)