from sklearn.svm import LinearSVC

# features (1 sim, 0 não)
# longo?
# perna curta?
# faz auau?

porco1 = [0, 1, 0]
porco2 = [0, 1, 1]
porco3 = [1, 1, 0]

cachorro1 = [0, 1, 1]
cachorro2 = [1, 0, 1]
cachorro3 = [1, 1, 1]

# 0 => cachorro, # 1 => porco
dados = [porco1, porco2, porco3, cachorro1, cachorro2, cachorro3]
classes = [1, 1, 1, 0, 0, 0]

model = LinearSVC()
model.fit(dados, classes)

animal_misterioso = [1, 1, 1]
# resultado1 [0] = cachorro
resultado1 = model.predict([animal_misterioso])

misterio1 = [1, 1, 1]
misterio2 = [1, 1, 0]
misterio3 = [0, 1, 1]

testes = [misterio1, misterio2, misterio3]
# previsoes [0 1 0] = cachorro, porco, cachorro
previsoes = model.predict(testes)

testes_classes = [0, 1, 1]

# [ True  True False] previsoes == testes_classes